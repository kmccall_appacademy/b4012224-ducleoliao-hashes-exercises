# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_length = {}
  str.split(" ").each do |word|
    word_length[word] = word.length
  end
  word_length
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|keys, values| values }[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_counts = Hash.new(0)
  word.each_char do |ch|
    letter_counts[ch] = word.count(ch)
  end
  letter_counts
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_hsh = {}
  arr.each do |el|
    uniq_hsh[el] = true
  end
  uniq_hsh.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  counts = {even: 0, odd: 0}
  numbers.each {|num| num.even? ? counts[:even] +=1 : counts[:odd] += 1 }
  counts
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = %w(a e i o u)
  vowel_counts = Hash.new(0)
  string.each_char do |letter|
      if vowels.include?(letter)
        vowel_counts[letter] += 1
      end
  end
  vowel_counts.sort_by {|k,v| v }.last.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  student_combine = []
  student_names = students.select {|names, months| months > 6}.keys
    student_names.each_with_index do |name, idx|
      (idx+1..student_names.length - 1).each do |idx2|
        student_combine << [name, student_names[idx2]]
      end
    end
    student_combine
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  biodiversity = Hash.new(0)
  specimens.each do |animal|
    biodiversity[animal] += 1
  end
  number_of_species = biodiversity.uniq.size
  smallest_population_size = biodiversity.values.min
  largest_population_size = biodiversity.values.max

  number_of_species ** 2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_counts = character_count(normal_sign)
  vandal_counts = character_count(vandalized_sign)
  vandal_counts.all? {|k,v| normal_counts[k] >= v}
end

def character_count(str)
  str.delete!(".,:;!?' ")
  ch_counts = Hash.new(0)
  str.each_char do |ch|
    ch_counts[ch.downcase] += 1
  end
  ch_counts
end
